<?php
/**
* Файл, содержащий класс MyException
* 
* Класс MyException наследуется от стандартного класса Exception.
* Этот класс записывает все ошибки в лог файл,а так же может выводить их на экран.
* @author Булат Камалов <bulatkamalov2008@yandex.ru>
* @author Алексей Кондратьев <>
* @version 1.0
* @package MyQueryBuilder
*/
/**
* Класс MyException
* @package MyQueryBuilder
*/
class MyException extends Exception 
{
	/**
	* @var string $err Содержит сообщение об ошибке
	*/
	public $err;
	/**
	* Функция конструктор.
	*
	* Функция получает сообщение ошибки,флаг debug, код ошибки и предыдущее исключение.
	* В конец лог файла будет записано сообщение об ошибке с датой, номером строки,
	* в которой ошибка, и файлом, в котором ошибка. В зависимости от флага debug
	* сообщение может выводится на экран.
	* @param string $message Параметр функции,в котором хранится сообщение ошибки
	* @param bool $debug Параметр функции,в котором хранится true или false, по умолчанию false
	* @param int $code Параметр функции,в котором хранится код ошибки
	* @param exception $previous Параметр функции,в котором хранится предыдущее исключение, по умолчанию null
	* @return void
	*/
	public function __construct($message,$debug = true, $code = 0, Exception $previous = null) 
	{
		parent::__construct($message, $code, $previous);
		$this->err = 'ERROR: '.$this->getMessage().' LINE: '.$this->getLine().'. FILE: '.$this->getFile();
		$this->message = date('d-m-Y H:i:s',time()).' '.$this->err."\n";
		if($debug)
		{
			echo '<b>'.$this->message.'<b/>';
		}
		@file_put_contents('logfile.log',$this->message,FILE_APPEND);
	}

}
?>