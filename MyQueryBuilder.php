<?php
/**
* Файл, содержащий класс MyQueryBuilder
* 
* Класс MyQueryBuilder последовательно строит запрос, путем вызова цепочки 
* методов. Внутри методов идет фильтрация типов,а так же отслеживается
* правильность построения запросов.
* @author Булат Камалов <bulatkamalov2008@yandex.ru>
* @author Алексей Кондратьев <>
* @version 1.0
* @package MyQueryBuilder
*/

/**
* Подключаем файл
*/
include ("MyException.php");

/**
* Класс MyQueryBuilder
* @package MyQueryBuilder
*/
class MyQueryBuilder
{
	/**
	* @var object $connect        Содержит PDO объект
	* @var string $sql            Содержит строку запроса
	* @var bool $rawqueryflag     Флаг по умолчанию false, становится true при вызове функции directQuery
	* @var bool $getconfflag      Флаг по умолчанию false, становится true при вызове функции getConfig
	* @var bool $debug            Флаг по умолчанию false, используется при 'выбрасывании' MyException
	* @var object|false $prepare  Содержит объект PDOStatment или же false
	* @var array $params          Содержит, передаваемые значения
	* @var object|false $result   Содержит объект PDOStatment или же false
	* @var string $errstring      Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errarray       Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errarraystring Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errstringint   Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errconditions  Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errorder       Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errconfig      Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errdb          Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errgetconf     Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnoselect    Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnofromset   Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnofrom      Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnolimit     Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnoinsert    Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errnoupdate    Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errfetch       Содержит сообщение об ошибке при 'выбрасывании' MyException
	* @var string $errfetchall    Содержит сообщение об ошибке при 'выбрасывании' MyException
	*/
	private $connect;
	private $sql = '';
	private $rawqueryflag = false;
	private $getconfflag = false;
	private $debug = true;
	private $prepare;
	private $params = array();
	private $result;
	private $errstring = 'Variable must be string.';
	private $errarray = 'Variable must be array.';
	private $errarraystring = 'Variables must be array or string.';
	private $errstringint = 'Variable must be integer or string.';
	private $errconditions = 'Variable must be condition.';
	private $errorder = 'Variable must be "DESC" or "ASC".';
	private $errconfig = 'Error reading config file.';
	private $errdb = 'Error connecting to database.';
	private $errgetconf = 'The function getConfig() has not been called.';
	private $errnoselect = 'The function select() has not been called.';
	private $errnofromset = 'The function from() or set() has not been called.';
	private $errnofrom = 'The function from() has not been called.';
	private $errnolimit = 'The function limit() has not been called.';
	private $errnoinsert = 'The function insert() has not been called.';
	private $errnoupdate = 'The function update() has not been called.';
	private $errfetch = 'Error in fetch().';
	private $errfetchall = 'Error in fetchAll().';
	
	
/*	public function __construct($db) 
	{
		
		$type = $db["type"];
		$login = $db["login"];
		$password = $db["password"];
		$host = $db["host"];
		$name = $db["name"];
		
		$this->connect = new PDO(
			"$type:host=$host;dbname=$name", //отловить эксепшн подключения
			$login,
			$password
		);
    }*/
	
	/**
	* Функция получения параметров  и подключения к базе данных
	*
	* Функция получает параметры подключения к базе данных из конфигурационного файла
	* и создает подключение к базе данных
	* @param string $conf Параметр функции,в котором хранится имя конфигурационного файла
	* @return object Возвращает экземпляр класса
	*/
	public function getConfig($conf)
	{
		if(!$config = parse_ini_file($conf))
		{
			throw new MyException($this->errconfig,$this->debug);
		}else
		{
			$this->getconfflag = true;
			$type = $config["type"];
			$login = $config["login"];
			$password = $config["password"];
			$host = $config["host"];
			$name = $config["name"];
			
			try
			{
				@$this->connect = new PDO("$type:host=$host;dbname=$name",$login,$password);
				return $this;
			}catch (Exception $e)
			{
				throw new MyException($this->errdb,$this->debug);
			}
		}
	}
	
	/**
	* Функция поиска строки
	*
	* Функция принимает строку,которую нужно найти в строке запроса,
	* и возвращает true или false
	* @param string $str Параметр функции,в котором хранится строка
	* @return bool Возвращает true или false
	*/
	private function check($str)
	{
		if(strpos($this->sql,$str)===false) return false;
		else return true;
	}

	/**
	* Функция select
	*
	* Функция принимает названия колонок базы данных,
	* которые могут быть типа string или array, и после
	* путем конкатенации добавляет названия колонок в строку запроса
	* @param string|array $columns Параметр функции,в котором хранится строка или массив строк
	* @return object Возвращает экземпляр класса
	*/
	public function select($columns) 
	{
		if(!$this->getconfflag) throw new MyException($this->errgetconf,$this->debug);
		$this->sql = 'SELECT';
		if (is_array($columns)) 
		{
			foreach ($columns as $column) 
			{
				if (is_string($column))
				{
					$this->sql = $this->sql.' '.trim(strip_tags($column)).',';					
				}else throw new MyException($this->errstring,$this->debug);
			}
			$this->sql = substr($this->sql,0,-1);
			return $this;
		}else if (is_string($columns)) 
		{
			$this->sql = $this->sql.' '.trim(strip_tags($columns));
			return $this;
		}else throw new MyException($this->errarraystring,$this->debug);
	}
	/**
	* Функция from
	*
	* Функция принимает название таблицы базы данных,
	* которое может быть типа string, и после
	* путем конкатенации добавляет название таблицы в строку запроса
	* @param string $table Параметр функции,в котором хранится строка
	* @return object Возвращает экземпляр класса
	*/
	public function from($table)
	{
		if($this->check('SELECT'))
		{
			$this->sql = $this->sql.' FROM';
			if (is_string($table)) 
			{
				$this->sql = $this->sql.' '.trim(strip_tags($table));
				return $this;
			}else throw new MyException($this->errstring,$this->debug);
		}else throw new MyException($this->errnoselect,$this->debug);
	}
	/**
	* Функция where
	*
	* Функция принимает два аргумента и знак условия.
	* Аргументы могут быть типа string или int,а знак условия только типа string.
	* Путем конкатенации добавляются аргументы и знак условия в строку запроса
	* @param string|int $arg1 Параметр функции,в котором хранится первый аргумент
	* @param string|int $arg2 Параметр функции,в котором хранится второй аргумент
	* @param string $condition Параметр функции,в котором хранится знак условия
	* @return object Возвращает экземпляр класса
	*/
	public function where($arg1,$condition,$arg2)
	{
		if($this->check('FROM') || $this->check('SET'))
		{
			$conditions = array('=','<>','>','>=','<','<=','LIKE');
			$this->sql = $this->sql.' WHERE';
			if(is_string($arg1) || is_integer($arg1))
			{
				if(is_string($condition))
				{
					if (in_array($condition,$conditions))
					{
						if(is_string($arg2) || is_integer($arg2))
						{
							//$this->sql = $this->sql.' '.trim(strip_tags($arg1)).trim(strip_tags($condition)).trim(strip_tags($arg2));
							$this->sql = $this->sql.' '.trim(strip_tags($arg1)).trim(strip_tags($condition)).'?';
							$this->params[] = $arg2;
							return $this;
						}else throw new MyException($this->errstringint,$this->debug);
					}else throw new MyException($this->errconditions,$this->debug);
				}else throw new MyException($this->errstring,$this->debug);
			}else throw new MyException($this->errstringint,$this->debug);
		}else throw new MyException($this->errnofromset,$this->debug);
	}
	/**
	* Функция orderBy
	*
	* Функция принимает название колонки и тип упорядочивания.
	* Колонки и тип упорядочивания могут быть типа string или типа array.
	* Тип упорядочивания можно не передавать.
	* Путем конкатенации добавляются название колонки и тип упорядочивания.
	* @param string $column Параметр функции,в котором хранится название колонки
	* @param string $order Параметр функции,в котором хранится тип упорядочивания
	* @return object Возвращает экземпляр класса
	*/
	public function orderBy($column,$order = '') 
	{	
		if($this->check('FROM'))
		{
			$this->sql = $this->sql.' ORDER BY';
			if(is_string($column) && is_string($order))
			{
				$this->sql = $this->sql.' '.$column;
				if(($order=='DESC') || ($order=="ASC") || ($order==""))
				{
					$this->sql = $this->sql." $order";
					return $this;
				}else throw new MyException($this->errorder,$this->debug);
			}else if(is_array($column) && is_array($order))
			{
				for ($i=0;$i<count($column);$i++)
				{
					$this->sql = $this->sql.' '.trim(strip_tags($column[$i])).' '.trim(strip_tags($order[$i])).',';
				}
				$this->sql = substr($this->sql,0,-1);
				return $this;
			}else if(is_array($column) && $order=='')
			{
				for ($i=0;$i<count($column);$i++)
				{
					$this->sql = $this->sql.' '.trim(strip_tags($column[$i])).',';
				}
				$this->sql = substr($this->sql,0,-1);
				return $this;	
			}throw new MyException($this->errarraystring,$this->debug);
		}throw new MyException($this->errnofrom,$this->debug);
	}
	/**
	* Функция offset
	*
	* Функция принимает параметр типа int или string, который должен быть числом.
	* Путем конкатенации значение параметра добавляется в строку запроса.
	* @param string|int $a Параметр функции,в котором хранится числовое значение
	* @return object Возвращает экземпляр класса
	*/
	public function offset($a)
	{
		if($this->check('LIMIT'))
		{
			$this->sql = $this->sql.' OFFSET';
			if(is_string($a) || is_integer($a))
			{
				$this->sql = $this->sql.' '.trim(strip_tags($a));
				return $this;
			}else throw new MyException($this->errstringint,$this->debug);
		}else throw new MyException($this->errnolimit,$this->debug);
	}
	/**
	* Функция limit
	*
	* Функция принимает параметр типа int или string, который должен быть числом.
	* Путем конкатенации значение параметра добавляется в строку запроса.
	* @param string|int $b Параметр функции,в котором хранится числовое значение
	* @return object Возвращает экземпляр класса
	*/
	public function limit($b) 
	{	
		$this->sql = $this->sql.' LIMIT';
		if(is_string($b) || is_integer($b))
		{
			$this->sql = $this->sql.' '.trim(strip_tags($b));
			return $this;
		}else throw new MyException($this->errstringint,$this->debug);
	}
	/**
	* Функция insert
	*
	* Функция принимает название таблицы и название колонки.
	* Название колонки можно не передавать.
	* Путем конкатенации название таблицы и название колонки добавляется в строку запроса.
	* @param string $table Параметр функции,в котором хранится числовое значение
	* @param string|array $columns Параметр функции,в котором хранится строка и массив строк
	* @return object Возвращает экземпляр класса
	*/
	public function insert($table,$columns='') 
	{
		if(!$this->getconfflag) throw new MyException($this->errgetconf,$this->debug);
		$this->sql = 'INSERT INTO';
		if(is_string($table)) 
		{
			$this->sql = $this->sql.' '.trim(strip_tags($table)).' (';
		}else throw new MyException($this->errstring,$this->debug);
		if(is_array($columns))
		{
			foreach ($columns as $field) 
			{
				if (is_string($field))
				{
					$this->sql = $this->sql.' '.trim(strip_tags($field)).',';
				}else throw new MyException($this->errstring,$this->debug);
			}
			$this->sql = substr($this->sql,0,-1);
			$this->sql = $this->sql.' )';
			return $this;
		}else if(is_string($columns))
		{
			$this->sql = $this->sql.' '.trim(strip_tags($columns)).' )';					
			return $this;
		}else throw new MyException($this->errarraystring,$this->debug);
	}
	/**
	* Функция values
	*
	* Функция принимает значения.
	* Путем конкатенации эти значения добавляются в строку запроса.
	* @param string|array $val Параметр функции,в котором хранится значение типа string или array
	* @return object Возвращает экземпляр класса
	*/
	public function values($val)
	{
		if($this->check('INSERT INTO'))
		{
			$this->sql = $this->sql.' VALUES (';
			if (is_array($val)) 
			{
				foreach ($val as $v) 
				{
					if (is_string($v))
					{
						$this->sql = $this->sql.' ? ,';
						array_push($this->params, $v);
					}else throw new MyException($this->errstring,$this->debug);
				}
				$this->sql = substr($this->sql,0,-1);
				$this->sql = $this->sql.' )';
				return $this;
			}else if(is_string($val))
			{
				$this->sql = $this->sql.' ? )';
				array_push($this->params, $v);
				return $this;
			}else throw new MyException($this->errarray,$this->debug);
		}else throw new MyException($this->errnoinsert,$this->debug);
	}
	/**
	* Функция delete
	*
	* Функция принимает название таблицы.
	* Путем конкатенации название таблицы добавляется в строку запроса.
	* @param string $table Параметр функции,в котором хранится название таблицы
	* @return object Возвращает экземпляр класса
	*/
	public function delete($table)
	{
		if(!$this->getconfflag) throw new MyException($this->errgetconf,$this->debug);
		$this->sql = 'DELETE FROM';
		if (is_string($table)) 
		{
			$this->sql = $this->sql.' '.trim(strip_tags($table));
			return $this;
		}else throw new MyException($this->errstring,$this->debug);	
	}
	/**
	* Функция update
	*
	* Функция принимает название таблицы.
	* Путем конкатенации название таблицы добавляется в строку запроса.
	* @param string $table Параметр функции,в котором хранится название таблицы
	* @return object Возвращает экземпляр класса
	*/
	public function update($table)
	{
		if(!$this->getconfflag) throw new MyException($this->errgetconf,$this->debug);
		$this->sql = 'UPDATE';
		if (is_string($table))
		{
			$this->sql = $this->sql.' '.trim(strip_tags($table));
			return $this;
		}else throw new MyException($this->errstring,$this->debug);	
	}
	/**
	* Функция set
	*
	* Функция принимает название колонки и значения, присваиваемые этим колонкам.
	* Путем конкатенации название колонки и значения добавляются в строку запроса.
	* @param string|array $columns Параметр функции,в котором хранится название колонки
	* @param string|array $values Параметр функции,в котором хранится значение, присваиваемое колонке
	* @return object Возвращает экземпляр класса
	*/
	public function set($columns,$values)
	{
		if($this->check('UPDATE'))
		{
			$this->sql = $this->sql.' SET';
			if(is_string($columns) && is_string($values)) 
			{
				//$this->sql = $this->sql.' '.trim(strip_tags($columns)).' = '.trim(strip_tags($values));
				$this->sql = $this->sql.' '.trim(strip_tags($columns)).' = ?';
				array_push($this->params, $values);
				return $this;
			}else if(is_array($columns) && is_array($values))
			{
				for ($i=0;$i<count($columns);$i++)
				{
					//$this->sql = $this->sql.' '.trim(strip_tags($columns[$i])).' = '.trim(strip_tags($values[$i])).',';
					$this->sql = $this->sql.' '.trim(strip_tags($columns[$i])).' = ? ,';
					array_push($this->params, $values[$i]);
				}
				$this->sql = substr($this->sql,0,-1);
				$this->sql = $this->sql;
				return $this;
			}else throw new MyException($this->errarraystring,$this->debug);
		}else throw MyException($this->errnoupdate,$this->debug);
	}
	/**
	* Функция directQuery
	*
	* Функция принимает полную строку запроса, и изменяет значение 
	* флага rawqueryflag на true.
	* @param string $sql Параметр функции,в котором хранится строка запроса
	* @return object Возвращает экземпляр класса
	*/
	public function directQuery($sql)
	{
		$this->sql = $sql;
		$this->rawqueryflag = true;
		return $this;
	}
	/**
	* Функция innerJoin
	*
	* Функция принимает названия присоединяемых таблиц и ключи, по которым они будут соединены.
	* Путем конкатенации названия присоединяемых таблиц и ключи добавляются в строку запроса.
	* @param string $tablefk Параметр функции,в котором хранится название таблицы с foreign key
	* @param string $tablepk Параметр функции,в котором хранится название таблицы с primary key
	* @param string $fk Параметр функции,в котором хранится foreign key
	* @param string $pk Параметр функции,в котором хранится primary key
	* @return object Возвращает экземпляр класса
	*/
	public function innerJoin($tablefk,$tablepk,$fk,$pk)
	{
		if($this->check('FROM'))
		{
			$this->sql = $this->sql.' INNER JOIN';
			if(is_string($tablefk) && is_string($fk) && is_string($pk) && is_string($tablepk)) 
			{
				$this->sql = $this->sql.' '.trim(strip_tags($tablepk)).' ON '.trim(strip_tags($tablefk)).'.'.trim(strip_tags($fk)).'='.
				trim(strip_tags($tablepk)).'.'.trim(strip_tags($pk));
			}else throw new MyException($this->errstring,$this->debug);
		}else throw new MyException($this->errnofrom,$this->debug);
	}
	/**
	* Функция leftJoin
	*
	* Функция принимает названия присоединяемых таблиц и ключи, по которым они будут соединены.
	* Путем конкатенации названия присоединяемых таблиц и ключи добавляются в строку запроса.
	* @param string $tablefk Параметр функции,в котором хранится название таблицы с foreign key
	* @param string $tablepk Параметр функции,в котором хранится название таблицы с primary key
	* @param string $fk Параметр функции,в котором хранится foreign key
	* @param string $pk Параметр функции,в котором хранится primary key
	* @return object Возвращает экземпляр класса
	*/
	public function leftJoin($tablefk,$tablepk,$fk,$pk)
	{
		if($this->check('FROM'))
		{
			$this->sql = $this->sql.' LEFT JOIN';
			if(is_string($tablefk) && is_string($fk) && is_string($pk) && is_string($tablepk)) 
			{
				$this->sql = $this->sql.' '.trim(strip_tags($tablepk)).' ON '.trim(strip_tags($tablefk)).'.'.trim(strip_tags($fk)).'='.
				trim(strip_tags($tablepk)).'.'.trim(strip_tags($pk));
			}else throw new MyException($this->errstring,$this->debug);
		}else throw new MyException($this->errnofrom,$this->debug);
	}
	/**
	* Функция rightJoin
	*
	* Функция принимает названия присоединяемых таблиц и ключи, по которым они будут соединены.
	* Путем конкатенации названия присоединяемых таблиц и ключи добавляются в строку запроса.
	* @param string $tablefk Параметр функции,в котором хранится название таблицы с foreign key
	* @param string $tablepk Параметр функции,в котором хранится название таблицы с primary key
	* @param string $fk Параметр функции,в котором хранится foreign key
	* @param string $pk Параметр функции,в котором хранится primary key
	* @return object Возвращает экземпляр класса
	*/
	public function rightJoin($tablefk,$tablepk,$fk,$pk)
	{
		if($this->check('FROM'))
		{
			$this->sql = $this->sql.' RIGHT JOIN';
			if(is_string($tablefk) && is_string($fk) && is_string($pk) && is_string($tablepk)) 
			{
				$this->sql = $this->sql.' '.trim(strip_tags($tablepk)).' ON '.trim(strip_tags($tablefk)).'.'.trim(strip_tags($fk)).'='.
				trim(strip_tags($tablepk)).'.'.trim(strip_tags($pk));
			}else throw new MyException($this->errstring,$this->debug);
		}else throw new MyException($this->errnofrom,$this->debug);
	}
	/**
	* Функция execute
	*
	* Функция выполняет прямой запрос или запрос из цепочки методов
	* в зависимости от значения флага rawqueryflag и сохраняет результат в переменную result
	* @return object Возвращает экземпляр класса
	*/
	public function execute()
	{	
		if($this->rawqueryflag) 
		{
			$this->result=$this->connect->query($this->sql);
			return $this;
		}
		else
		{
			$this->prepare = $this->connect->prepare($this->sql);
			$this->result = $this->prepare->execute($this->params);
			if(!$this->result)
			{	
				$err = implode(',',$this->prepare->errorInfo()); 
				throw new MyException("$err",$this->debug);
			}else return $this;
		}
	}
	/**
	* Функция result
	*
	* Функция выводит на экран результат выполнения запроса
	* @return void
	*/
	public function result()
	{
		if($this->check('SELECT'))
		{
			if($this->rawqueryflag) 
			{
				if($row = $this->result->fetch())
				{
					echo '<br>';
					$keys=array_keys($row);
					$i=0;
					for($j=0;$j<count($row)/2;$j++)
					{
						echo $keys[$i*2].'='.$row[$i].' ';
						$i++;
					}
				}else throw new MyException($this->errfetch,$this->debug);
			}else
			{
				if($row = $this->prepare->fetchAll(PDO::FETCH_ASSOC))
				{
					echo '<br>';
					$keys=array_keys($row[0]);
					for($j=0;$j<count($row);$j++)
					{
						for($i=0;$i<count($row[0]);$i++)
						{
							echo $keys[$i].' = '.$row[$j]["$keys[$i]"].' ';
						}	
						echo '<br>';
					}
				}else throw new MyException($this->errfetchall,$this->debug);
			}
		}else throw new MyException($this->errnoselect,$this->debug);
	}
}
?>
